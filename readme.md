# Ansible Role Cisco SNMP Config

This is a Ansible Role for setting SNMP Settings on Cisco Devices.

Aufbau:
- Build Cisco Config
  - All Availiable Build Modules
    - All ansible modes
    - tags: build
  - Cisco IOS CLI
    - All ansible modes
    - tags: 
      - build
      - cli
- Testing Device Config
  - Snapshot of Device Running Config and write to Startup
    - All ansible modes
    - tags:
      - bak
      - state
  - Snapshot of Device state Config
    - All ansible modes
    - tags:
      - bak
      - state
  - Review Config changes (--diff)
    - All ansible modes
    - tags:
      - diff
      - bak
      - state
      - tests
  - Testing Config changes
    - All Ansible modes
    - tags:
      - tests
- Deploy Device Config
  - Deploy Config with CLI
    - not ansible check-mode
    - tags:
      - deploy
      - cli
      - oam 

Requirements:
    None

Role Variables:
    - snmp_community
    - snmp_location
    - net_oam_vlan

Using this Role:
Drive to Ansible Role Directory:
    - git clone https://fhzengitlab1.fhooe.at/P50010/cisco-snmp.git

Aktivate Role in a Playbook:

Example:
```YAML
- hosts: all
  roles:
     - cisco-snmp
```

Tested:
 - Cisco IOS
 - Cisco IOS-XE
 
License:
    MIT / BSD

Author Information:
roland@stumpner.at